<?php if (!empty($title)) : ?><h3><?php print $title; ?></h3><?php endif; ?>
<div class="cycle-slideshow" <?php print $options['data_attr'];?>>
	<?php	foreach ($rows as $id => $row) : ?>
	<div class="cycle-slide"><?php print $row; ?></div>
	<?php endforeach; ?>

  <?php if ($options['slider_nav'] || $options['slider_pager']) : ?><div class="cycle-control">
    <?php if ($options['slider_nav']) : ?>
      <span class="cycle-prev"><?php print $options['slider_prevtext']; ?></span><?php endif; ?>
    <?php if ($options['slider_pager']) : ?><span class="cycle-pager"></span><?php endif; ?>
    <?php if ($options['slider_nav']) : ?><span class="cycle-next"><?php print $options['slider_nexttext']; ?></span><?php endif; ?>
  </div><?php endif; ?>
</div>
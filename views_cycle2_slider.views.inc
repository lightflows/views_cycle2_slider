<?php

function views_cycle2_slider_views_plugins() {
  return array(
    'style' => array(
      'views_cycle2_slider' => array(
        'title' => t('Cycle2 Slider'),
        'handler' => 'views_cycle2_slider_style_plugin',
        'type' => 'normal',
        'uses row plugin' => true,
        'uses options' => true,
        'theme' => 'views-cycle2-slider-style'
      ),
    ),
  );
}

<?php

class views_cycle2_slider_style_plugin extends views_plugin_style_list {
  private function load_js($path) {
      if (file_exists($path . '/jquery.cycle2.js')) {
        drupal_add_js($path . '/jquery.cycle2.js');
      } else {
        if (file_exists($path . '/jquery.cycle2.min.js')) {
          drupal_add_js($path . '/jquery.cycle2.min.js');
        }
      }
  }

  function pre_render($result) {
    parent::pre_render($result);

    if ($path = libraries_get_path('jquery.cycle2')) {
      $this->load_js($path);
    } else {
      if ($path = libraries_get_path('cycle2')) {
        $this->load_js($path);
      }
    }

    // Generate data-cycle-* attributes from the options

    $data_attributes = array(
      'fx' => $this->options['slider_fx'],
      'pause-on-hover' => $this->options['slider_pause'],
      'swipe' => $this->options['slider_swipe'],
      'timeout' => $this->options['slider_timeout'],

      'log' => 'false',
      'pager' => '> .cycle-control > .cycle-pager',
      'prev' => '> .cycle-control > .cycle-prev',
      'next' => '> .cycle-control > .cycle-next',
      'slides' => '> .cycle-slide',
    );

    $this->options['data_attr'] = '';

    foreach ($data_attributes as $key => $value) {
      $this->options['data_attr'] .= "data-cycle-$key=\"$value\" ";
    }
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['slider_timeout'] = array('default' => '8000');
    $options['slider_pause'] = array('default' => '1');
    $options['slider_pager'] = array('default' => '0');
    $options['slider_nav'] = array('default' => '0');
    $options['slider_prevtext'] = array('default' => 'Previous');
    $options['slider_nexttext'] = array('default' => 'Next');
    $options['slider_swipe'] = array('default' => '1');
    $options['slider_fx'] = array('default' => 'fade');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['slider_timeout'] = array(
      '#type' => 'textfield',
      '#title' => t('Timeout'),
      '#description' => t('Time between slide transitions, in milliseconds'),
      '#default_value' => $this->options['slider_timeout'],
    );

    $form['slider_pause'] = array(
      '#type' => 'select',
      '#title' => t('Pause on hover'),
      '#options' => array(
        0 => t('False'),
        1 => t('True')
      ),
      '#default_value' => $this->options['slider_pause'],
    );

    $form['slider_pager'] = array(
      '#type' => 'select',
      '#title' => t('Show pager'),
      '#options' => array(
        0 => t('False'),
        1 => t('True')
      ),
      '#default_value' => $this->options['slider_pager'],
    );

    $form['slider_nav'] = array(
      '#type' => 'select',
      '#title' => t('Show navigation'),
      '#options' => array(
        0 => t('False'),
        1 => t('True')
      ),
      '#default_value' => $this->options['slider_nav'],
    );

    $form['slider_prevtext'] = array(
      '#type' => 'textfield',
      '#title' => t('Text for the "previous" button'),
      '#default_value' => $this->options['slider_prevtext'],
    );

    $form['slider_nexttext'] = array(
      '#type' => 'textfield',
      '#title' => t('Text for the "next" button'),
      '#default_value' => $this->options['slider_nexttext'],
    );

    $form['slider_swipe'] = array(
      '#type' => 'select',
      '#title' => t('Swipe'),
      '#description' => t('Enable swipe navigation'),
      '#options' => array(
        0 => t('False'),
        1 => t('True')
      ),
      '#default_value' => $this->options['slider_swipe'],
    );

    $form['slider_fx'] = array(
      '#type' => 'select',
      '#title' => t('Transition effect'),
      '#options' => array(
        'none' => t('None'),
        'fade' => t('Fade'),
        'fadeout' => t('Fadeout'),
        'scrollHorz' => t('Horizontal scroll')
      ),
      '#default_value' => $this->options['slider_fx'],
    );
  }
}
